package main

import (
	"github.com/mymmrac/telego"
	"github.com/sirupsen/logrus"
	"os"
	"telegram_random_picture/internal/config"
	"telegram_random_picture/internal/handler"
	logger "telegram_random_picture/internal/logging"
)

var bot *telego.Bot
var l *logrus.Logger

func main() {
	l = logger.GetLogger()
	cfg := config.GetConfig()
	botToken := cfg.TelegramApiKey

	var err error
	bot, err = telego.NewBot(botToken, telego.WithDefaultDebugLogger())
	if err != nil {
		l.Errorf("Can't create bot: %s", err)
		os.Exit(1)
	}
	initBotCommands()
}
func initBotCommands() {
	var categories map[string]string = make(map[string]string)

	categories[handler.CategoryCat] = "Котики"
	categories[handler.CategoryDark] = "Темные фото"
	categories[handler.CategoryFood] = "Еда"
	categories[handler.CategoryMountain] = "Горы"
	categories[handler.CategorySpace] = "Космос"
	categories[handler.CategoryBeach] = "Пляж"
	categories[handler.CategoryBusiness] = "Бизнес"
	categories[handler.CategoryNature] = "Природа"
	categories[handler.CategoryFlowers] = "Цветы"
	categories[handler.CategoryForest] = "Лес"
	categories[handler.CategoryLandscape] = "Пейзаж"
	categories[handler.CategoryCity] = "Города"
	categories[handler.CategoryCar] = "Автомобили"
	categories[handler.CategorySunset] = "Закат"
	categories[handler.CategoryAbstract] = "Абстракция"
	categories[handler.CategorySpring] = "Весна"
	categories[handler.CategoryTechnology] = "Технологии"
	categories[handler.CategorySky] = "Небо"

	var commands []telego.BotCommand
	for key, value := range categories {
		commands = append(commands,
			telego.BotCommand{
				Command:     key,
				Description: value,
			},
		)
	}

	params := &telego.SetMyCommandsParams{
		Commands: commands,
	}
	err := bot.SetMyCommands(params)
	if err != nil {
		l.Errorf("Error on add commands: %s", err)
	}
}
