package main

import (
	"github.com/mymmrac/telego"
	th "github.com/mymmrac/telego/telegohandler"
	"os"
	"telegram_random_picture/internal/config"
	"telegram_random_picture/internal/handler"
	logger "telegram_random_picture/internal/logging"
)

func main() {
	l := logger.GetLogger()
	cfg := config.GetConfig()

	botToken := cfg.TelegramApiKey

	bot, err := telego.NewBot(botToken, telego.WithDefaultDebugLogger())
	if err != nil {
		l.Errorf("Can't create bot: %s", err)
		os.Exit(1)
	}

	// Set up a webhook on Telegram side
	_ = bot.SetWebhook(&telego.SetWebhookParams{
		URL: cfg.MainDomain + "/bot" + bot.Token(),
	})

	// Receive information about webhook
	info, _ := bot.GetWebhookInfo()
	l.Infof("Webhook Info: %+v\n", info)

	// Start server for receiving requests from the Telegram
	go func() {
		_ = bot.StartWebhook("0.0.0.0:80")
	}()

	// Get an update channel from webhook.
	// (more on configuration in examples/updates_webhook/main.go)
	updates, _ := bot.UpdatesViaWebhook("/bot" + bot.Token())

	// Stop reviving updates from update channel and shutdown webhook server
	defer func() {
		_ = bot.StopWebhook()
	}()

	// Create bot handler and specify from where to get updates
	bh, _ := th.NewBotHandler(bot, updates)
	// Stop handling updates
	defer bh.Stop()

	// Register new handler with match on command `/start`
	bh.Handle(handler.SendPicture(l), th.Union(
		th.CommandEqual(handler.CategoryCat),
		th.CommandEqual(handler.CategoryDark),
		th.CommandEqual(handler.CategoryFood),
		th.CommandEqual(handler.CategoryMountain),
		th.CommandEqual(handler.CategorySpace),
		th.CommandEqual(handler.CategoryBeach),
		th.CommandEqual(handler.CategoryBusiness),
		th.CommandEqual(handler.CategoryNature),
		th.CommandEqual(handler.CategoryFlowers),
		th.CommandEqual(handler.CategoryForest),
		th.CommandEqual(handler.CategoryLandscape),
		th.CommandEqual(handler.CategoryCity),
		th.CommandEqual(handler.CategoryCar),
		th.CommandEqual(handler.CategorySunset),
		th.CommandEqual(handler.CategorySpring),
		th.CommandEqual(handler.CategoryAbstract),
		th.CommandEqual(handler.CategoryTechnology),
		th.CommandEqual(handler.CategorySky),
	))
	bh.Start()
}
