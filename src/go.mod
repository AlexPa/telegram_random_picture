module telegram_random_picture

go 1.20

require (
	github.com/ilyakaznacheev/cleanenv v1.4.2
	github.com/mymmrac/telego v0.22.0
	github.com/sirupsen/logrus v1.9.0
)

require (
	github.com/BurntSushi/toml v1.1.0 // indirect
	github.com/andybalholm/brotli v1.0.4 // indirect
	github.com/fasthttp/router v1.4.17 // indirect
	github.com/goccy/go-json v0.10.1 // indirect
	github.com/joho/godotenv v1.4.0 // indirect
	github.com/klauspost/compress v1.15.9 // indirect
	github.com/savsgio/gotils v0.0.0-20230208104028-c358bd845dee // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	github.com/valyala/fasthttp v1.44.0 // indirect
	golang.org/x/sys v0.6.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
	olympos.io/encoding/edn v0.0.0-20201019073823-d3554ca0b0a3 // indirect
)
