package config

import (
	"github.com/ilyakaznacheev/cleanenv"
	"sync"
)

type Config struct {
	TelegramApiKey string `yaml:"telegram_api_key"`
	PixelApiKey    string `yaml:"pixel_api_key"`
	PixelBaseUrl   string `yaml:"pixel_base_url"`
	MainDomain     string `yaml:"main_domain"`
}

var instance *Config
var once sync.Once

func GetConfig() *Config {
	once.Do(func() {
		instance = &Config{}
		err := cleanenv.ReadConfig("config.yml", instance)
		if err != nil {
			desc, _ := cleanenv.GetDescription(instance, nil)
			panic(desc)
		}
	})
	return instance
}
