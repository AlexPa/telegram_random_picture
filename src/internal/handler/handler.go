package handler

import (
	"github.com/mymmrac/telego"
	"github.com/mymmrac/telego/telegohandler"
	tu "github.com/mymmrac/telego/telegoutil"
	"github.com/sirupsen/logrus"
	"strings"
	"telegram_random_picture/internal/config"
	"telegram_random_picture/internal/pixels"
)

const (
	CategoryCat        = "cat"
	CategoryDark       = "dark"
	CategoryFood       = "food"
	CategoryMountain   = "mountain"
	CategorySpace      = "space"
	CategoryBeach      = "beach"
	CategoryBusiness   = "business"
	CategoryNature     = "nature"
	CategoryFlowers    = "flowers"
	CategoryForest     = "forest"
	CategoryLandscape  = "landscape"
	CategoryCity       = "city"
	CategoryCar        = "car"
	CategorySunset     = "sunset"
	CategorySpring     = "spring"
	CategoryAbstract   = "abstract"
	CategoryTechnology = "technology"
	CategorySky        = "sky"
)

func getRandomImage(logger *logrus.Logger, category string) *pixels.PixelSinglePhotoResponse {
	cfg := config.GetConfig()
	baseUrl := cfg.PixelBaseUrl
	apiKey := cfg.PixelApiKey
	api := pixels.NewPixelApi(logger, baseUrl, apiKey)

	pixel := pixels.NewPixel(api, logger)
	image := pixel.GetRandomImage(category)
	return image
}

func SendPicture(l *logrus.Logger) telegohandler.Handler {
	return func(bot *telego.Bot, update telego.Update) {
		cmd := update.Message.Text
		cmd = strings.Replace(cmd, "/", "", -1)

		chatID := update.Message.Chat.ID
		image := getRandomImage(l, cmd)
		if image != nil {
			l.Debugf("Image is: %v", image)
			// 5mb for photo restriction
			photo := tu.Photo(
				tu.ID(chatID),
				tu.FileByURL(image.Src.Large),
			).WithCaption(image.Alt)
			_, err := bot.SendPhoto(photo)

			//but 20 for other docs restriction
			doc := tu.Document(
				tu.ID(chatID),
				tu.FileByURL(image.Src.Original),
			).WithCaption("Download original size")
			_, err = bot.SendDocument(doc)

			if err != nil {
				l.Errorf("Can't send image %s: %v", image.Src.Original, err)
			}
		}
	}
}
