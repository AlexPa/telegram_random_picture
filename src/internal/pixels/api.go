package pixels

import (
	"fmt"
	"github.com/sirupsen/logrus"
	"io"
	"net/http"
	"net/url"
	"time"
)

var PixelUnknownError = NewPixelError("some_unique_code", "Pixel returned status not equals 200")

var httpCient = &http.Client{Timeout: 20 * time.Second}

type PixelSinglePhotoSizesResponse struct {
	Original  string `json:"original"`
	Large2x   string `json:"large2X"`
	Large     string `json:"large"`
	Medium    string `json:"medium"`
	Small     string `json:"small"`
	Portrait  string `json:"portrait"`
	Landscape string `json:"landscape"`
	Tiny      string `json:"tiny"`
}
type PixelSinglePhotoResponse struct {
	Id              int                           `json:"id"`
	Width           int                           `json:"width"`
	Height          int                           `json:"height"`
	Url             string                        `json:"url"`
	Photographer    string                        `json:"photographer"`
	PhotographerUrl string                        `json:"photographer_url"`
	PhotographerId  int                           `json:"photographer_id"`
	AvgColor        string                        `json:"avg_color"`
	Src             PixelSinglePhotoSizesResponse `json:"src"`
	Linked          bool                          `json:"linked"`
	Alt             string                        `json:"alt"`
}
type PixelSearchResponse struct {
	TotalResults int                        `json:"total_results"`
	Page         int                        `json:"page"`
	PerPage      int                        `json:"per_page"`
	Photos       []PixelSinglePhotoResponse `json:"photos"`
}
type PixelError struct {
	Code              string
	Message           string
	PixelResponseCode int
}

func (err PixelError) Error() string {
	return err.Message
}

func NewPixelError(code, message string) error {
	return &PixelError{
		Code:    code,
		Message: message,
	}
}

type PixelApi struct {
	logger   *logrus.Logger
	BaseUrl  string
	ApiKey   string
	per_page string
}

func NewPixelApi(logger *logrus.Logger, baseUrl, apiKey string) *PixelApi {
	return &PixelApi{
		logger:   logger,
		BaseUrl:  baseUrl,
		ApiKey:   apiKey,
		per_page: "80",
	}
}
func (api PixelApi) Call(urlPart string, params *url.Values) ([]byte, error) {
	params.Add("per_page", api.per_page)
	var urlRequest string
	if params == nil {
		urlRequest = fmt.Sprintf("%s/%s", api.BaseUrl, urlPart)
	} else {
		urlRequest = fmt.Sprintf("%s/%s?%s", api.BaseUrl, urlPart, params.Encode())
	}
	logrus.Info(urlRequest)
	req, err := http.NewRequest(http.MethodGet, urlRequest, nil)
	if err != nil {
		api.logger.Errorf("Error while creating request: %s", err)
	}
	req.Header.Set("Authorization", api.ApiKey)
	res, err := httpCient.Do(req)
	defer res.Body.Close()
	if err != nil {
		api.logger.Errorf("Error while doing DO request: %s", err)
	}
	api.logger.Debugf("Request status code: %d", res.StatusCode)

	var bytesArray []byte
	bytesArray, err = io.ReadAll(res.Body)
	if err != nil {
		api.logger.Errorf("Error while reading bytes: %s", err)
	}
	if res.StatusCode == http.StatusOK {
		return bytesArray, nil
	}

	return nil, PixelUnknownError
}
